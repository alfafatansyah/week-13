#include "stm32f411xe.h"
#include "Delay_F103.h"

void gpio_config (void)
{
	/************** STEPS TO FOLLOW *****************
	1. Enable the GPIOC clcok
	2. Congifure the Pin as output
	************************************************/
	
	RCC->APB1ENR |= (1<<2); // Enable GPIO Clock
	
	GPIOC->MODER |= (1<<26); // PC13 Output Mode, push pull
	GPIOC->OTYPER = 0;
	GPIOC->OSPEEDR = 0;	
}

int main()
{
	SystemInit();
	gpio_config();
	TIM2_Config();
	
	while(1)
	{
		GPIOC->BSRR |= (1<<13); // Set the pin PC13
		Delay_ms (1000);
		GPIOC->BSRR |= ((1<<13) <<16); // Reset pin PC13
		Delay_ms (1000);
	}
}