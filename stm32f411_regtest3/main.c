#include "stm32f411xe.h"

void sys_clock_config (void);
void gpio_config (void);
void delay (uint32_t time);

int main (void)
{
	sys_clock_config();
	gpio_config();
	
	while(1)
	{
		GPIOB->BSRR |= (1<<10); // Set the pin PB10
		delay (1000000);
		GPIOB->BSRR |= ((1<<10) <<16); // Reset pin PB10
		delay (1000000);
		GPIOB->BSRR |= (1<<1); // Set the pin PB10
		delay (1000000);
		GPIOB->BSRR |= ((1<<1) <<16); // Reset pin PB10
		delay (1000000);
		GPIOC->BSRR |= (1<<13); // Set the pin PC13
		delay (1000000);
		GPIOC->BSRR |= ((1<<13) <<16); // Reset pin PC13
		delay (1000000);
	}
}

void sys_clock_config (void)
{	
	/****************>>>>>>> STEPS FOLLOWED <<<<<<<<****************
	
	1. ENABLE HSE and ait for the HSE to become Ready
	2. Set the POWER ENABLE CLOCK and VOLTAGE REGULATOR
	3. Configure the FLASH PREFETCH and the LATENCY Related Settings
	4. Configure the PRESCALARS HCLK, PCLK1, PCLK2
	5. Configure the MAIN PLL
	6. Enable the PLL and wait for it to become ready
	7. Select the Clock Source and wait for it to be set
	
	****************************************************************/
	
	#define PLL_M 12
	#define PLL_N 96
	#define PLL_P 0 // PPLP = 2
	
	// 1. ENABLE HSE and ait for the HSE to become Ready
	RCC->CR |= (RCC_CR_HSEON);
	while(!(RCC->CR & RCC_CR_HSERDY));
	
	// 2. Set the POWER ENABLE CLOCK and VOLTAGE REGULATOR
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR |= PWR_CR_VOS;
	
	// 3. Configure the FLASH PREFETCH and the LATENCY Related Settings
	FLASH->ACR |= FLASH_ACR_DCEN | FLASH_ACR_ICEN | FLASH_ACR_PRFTEN | FLASH_ACR_LATENCY_3WS;

	// 4. Configure the PRESCALARS HCLK, PCLK1, PCLK2
	// AHB PR
	RCC->CFGR |= RCC_CFGR_HPRE_DIV1;
	
	// APB1 PR
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;
	
	// APB2 PR
	RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;
	
	// 5. Configure the MAIN PLL
	RCC->PLLCFGR = (PLL_M <<0) | (PLL_N <<6) | (PLL_P <<16) | (RCC_PLLCFGR_PLLSRC_HSE);
	
	// 6. Enable the PLL and wait for it to become ready
	RCC->CR |= RCC_CR_PLLON;
	while(!(RCC->CR & RCC_CR_PLLRDY));
	
	// 7. Select the Clock Source and wait for it to be set
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);
}

void gpio_config (void)
{
	/****** STEPS FOLLOWED ******
	
	1. Enable the GPIOA CLOCK
	2. Set the Pin as OUTPUT
	3. Configure the OUTPUT MODE
	
	****************************/
	
	// 1. Enable the GPIOB CLOCK
	RCC->AHB1ENR |= (1<<1);
	
	// 2. Set the Pin as OUTPUT
	GPIOB->MODER |= (1<<20); // pin PB10(bits 21:20) as Output (01)
	
	// 3. Configure the OUTPUT MODE
	GPIOB->OTYPER = 0;
	GPIOB->OSPEEDR = 0;	
	
	// 1. Enable the GPIOB CLOCK
	RCC->AHB1ENR |= (1<<1);
	
	// 2. Set the Pin as OUTPUT
	GPIOB->MODER |= (1<<2); // pin PB10(bits 3:2) as Output (01)
	
	// 3. Configure the OUTPUT MODE
	GPIOB->OTYPER = 0;
	GPIOB->OSPEEDR = 0;	
	
	// 1. Enable the GPIOC CLOCK
	RCC->AHB1ENR |= (1<<2);
	
	// 2. Set the Pin as OUTPUT
	GPIOC->MODER |= (1<<26); // pin PB10(bits 27:26) as Output (01)
	
	// 3. Configure the OUTPUT MODE
	GPIOC->OTYPER = 0;
	GPIOC->OSPEEDR = 0;	
}

void delay (uint32_t time)
{
	while (time--);
}