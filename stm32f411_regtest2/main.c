#include "stm32f411xe.h"

void sys_clock_config (void);
void gpio_config (void);
void delay (uint32_t time);

int main (void)
{
	sys_clock_config();
	gpio_config();
	
	while(1)
	{
		GPIOB->BSRR |= (1<<10);
		delay (1000000);
		GPIOB->BSRR |= ((1<<10) <<16);
		delay (1000000);
	}
}

void sys_clock_config (void)
{	
	#define PLL_M 12
	#define PLL_N 96
	#define PLL_P 0
	
	RCC->CR |= (RCC_CR_HSEON);
	while(!(RCC->CR & RCC_CR_HSERDY));
	
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CR |= PWR_CR_VOS;
	
	FLASH->ACR |= FLASH_ACR_DCEN | FLASH_ACR_ICEN | FLASH_ACR_PRFTEN | FLASH_ACR_LATENCY_3WS;

	RCC->CFGR |= RCC_CFGR_HPRE_DIV1;
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;
	RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;
	
	RCC->PLLCFGR = (PLL_M <<0) | (PLL_N <<6) | (PLL_P <<16) | (RCC_PLLCFGR_PLLSRC_HSE);
	
	RCC->CR |= RCC_CR_PLLON;
	while(!(RCC->CR & RCC_CR_PLLRDY));
	
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);
}

void gpio_config (void)
{
	RCC->AHB1ENR |= (1<<1);
	GPIOB->MODER |= (1<<20);
	GPIOB->OTYPER = 0;
	GPIOB->OSPEEDR = 0;	
}

void delay (uint32_t time)
{
	while (time--);
}