#include "stm32f411xe.h"
#include "Delay_F103.h"

void gpio_config (void)
{
	/****** STEPS FOLLOWED ******
	
	1. Enable the GPIOA CLOCK
	2. Set the Pin as OUTPUT
	3. Configure the OUTPUT MODE
	
	****************************/
	
	// 1. Enable the GPIOC CLOCK
	RCC->AHB1ENR |= (1<<2);
	
	// 2. Set the Pin as OUTPUT
	GPIOC->MODER |= (1<<26); // pin PB10(bits 27:26) as Output (01)
	
	// 3. Configure the OUTPUT MODE
	GPIOC->OTYPER = 0;
	GPIOC->OSPEEDR = 0;	
}

int main()
{
	SystemInit();
	gpio_config();
	TIM2_Config();
	while(1)
	{
		GPIOC->BSRR |= (1<<13); // Set the pin PC13
		Delay_ms (1000);
		GPIOC->BSRR |= ((1<<13) <<16); // Reset pin PC13
		Delay_ms (1000);
	}
}