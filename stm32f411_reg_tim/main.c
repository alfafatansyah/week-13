#include "RccConfig.h"

void tim2_config (void)
{
	/******************* STEPS FOLLOWED ********************	
	1. Enable Timer clock
	2. Set the prescaler and the ARR
	3. Enable the Timer, and wait for the update Flag to set	
	********************************************************/
	
	// 1. Enable Timer clock
	RCC->APB1ENR |= (1<<0); // Enable the timer2 clock
	
	// 2. Set the prescaler and the ARR
	TIM2->PSC = 100-1; // 100MHz/100 = 1MHz -- 1 us delay
	TIM2->ARR = 0xffff-1; // MAX ARR value
	
	// 3. Enable the Timer, and wait for the update Flag to set
	TIM2->CR1 |= (1<<0); // Enable the Counter
	while (!(TIM2->SR & (1<<0))); // UIF: Update interrupt flag.. This bit is set by hardware when registers are update
}

void delay_us (uint16_t us)
{
	/******************* STEPS FOLLOWED ********************	
	1. Reset the Counter
	2. Wait for the Counter to reach the entered value. As each count will take 1 us,
		 the total waiting time will be the required us delay	
	********************************************************/
	TIM2->CNT = 0;
	while(TIM2->CNT < us);
}

void delay_ms (uint16_t ms)
{
	for (uint16_t i=0; i<ms; i++)
	{
		delay_us(1000); // delay of 1 ms
	}
}

void gpio_config (void)
{
	/****** STEPS FOLLOWED ******	
	1. Enable the GPIOC CLOCK
	2. Set the Pin as OUTPUT
	3. Configure the OUTPUT MODE	
	****************************/
	
	// 1. Enable the GPIOC CLOCK
	RCC->AHB1ENR |= (1<<2);
	
	// 2. Set the Pin as OUTPUT
	GPIOC->MODER |= (1<<26); // pin PC10(bits 27:26) as Output (01)
	
	// 3. Configure the OUTPUT MODE
	GPIOC->OTYPER &= ~(1<<13); // bit 13=0 --> Output push pull
	GPIOC->OSPEEDR |= (1<<27); // Pin PC13 (bits 27:26) as Fast Speed (1:0)
	GPIOC->PUPDR &= ~((1<<26) | (1<<27)); // Pin PC13 (bits 27:26) are 0:0 --> no pull up or pulldown
}

int main ()
{
	SysClockConfig();
	tim2_config();
	gpio_config();
	
	while(1)
	{
		GPIOC->BSRR |= (1<<13); // Set the pin PC13
		delay_ms (500);
		GPIOC->BSRR |= ((1<<13) <<16); // Reset pin PC13
		delay_ms (500);
	}
}