#include "RccConfig.h"
#include "delay.h"

void uart_config (void)
{
	/********************************* STEPS FOLLOWED *********************************
	1. Enable the UART CLOCK GPIO CLOCK
	2. Configure the UART PINs for Alternate Function	
	3. Enable the USART by writing the UE bit in USART_CR1 register to 1.
	4. Program the M bit in USART_CR1 to define the word length.
	5. Select the desired baud rate using USART_BRR register.
	6. Enable the Transmitter/Receiver by setting the TE and RF bits in USART_CR1 Register
	***********************************************************************************/
	
	// 1. Enable the UART CLOCK GPIO CLOCK
	RCC->APB2ENR |= (1<<4); // Enable UART1 CLOCK
	RCC->AHB1ENR |= (1<<0); // Enable GPIO CLOCK
	
	// 2. Configure the UART PINs for Alternate Function	
	GPIOA->MODER |= (2<<18); // Bits (19:18)= 1:0 --> Alternate Function for Pin PA9
	GPIOA->MODER |= (2<<20); // Bits (21:20)= 1:0 --> Alternate Function for Pin PA10
	
	GPIOA->OSPEEDR |= (3<<18) | (3<<20); // Bits (19:28)= 1:1 and Bits (21:20)= 1:1 --> high Speed for PIN PA9 and PA10
	
	GPIOA->AFR[1] |= (7<<4); // Bytes (7:6:5:4) = 0:1:1:1 --> AF7 Alternate function for USART1 at Pin PA9
	GPIOA->AFR[1] |= (7<<8); // Bytes (11:10:9:8) = 0:1:1:1 --> AF7 Alternate function for USART1 at Pin PA10
	
	// 3. Enable the USART by writing the UE bit in USART_CR1 register to 1.
	USART1->CR1 = 0X00; // clear all
	USART1->CR1 |= (1<<13); // UE = 1... Enable USART
	
	// 4. Program the M bit in USART_CR1 to define the word length.
	USART1->CR1 &= ~(1<<12); // M = 0; 0 bit word length
	
	// 5. Select the desired baud rate using USART_BRR register.
	USART1->BRR = (4<<0) | (54<<4); // Baud rate of 115200, PCLK2 at 100MHz
	
	// 6. Enable the Transmitter/Receiver by setting the TE and RF bits in USART_CR1 Register
	USART1->CR1 |= (1<<2); // RE=1.. Enable the Receiver
	USART1->CR1 |= (1<<3); // TE=1.. Enable Transmitter	
}

void uart1_send_char (uint8_t c)
{
	/********************************* STEPS FOLLOWED *********************************
	1. Write the data to send in the USART_DR register (this clears the TXE bit). Repeat this
		 for each data to be transmitted in case of single buffer.
	2. After writing the last data into the USART_DE register, wait until TC=1. This indicates
		 that the transmission of the last frame is complete. This required for instance when
		 the USART is disabled or enters the Halt mode to avoid corrupting the transmission.
	***********************************************************************************/
	
	USART1->DR = c; // load the data into DR register
	while(!(USART1->SR & (1<<6))); // Wait for TC to SET.. This indicates that the data has been transmitted.
}

void uart1_send_string (char *string)
{
	while (*string) uart1_send_char (*string++);
}

uint8_t uart_get_char (void)
{
	/********************************* STEPS FOLLOWED *********************************
	1. Wait for the RXNE bit to set. It indicates that the data has been received and can be read.
	2. Read the data from USART_DR Register. This also clears the RXNE bit
	***********************************************************************************/

	uint8_t temp;
	
	while (!(USART1->SR & (1<<5))); // wait for RXNE bit to set
	temp = USART1->DR; // read the data. This clears the RXNE also
	return temp;
}

int main()
{
	SysClockConfig();
	tim2_config();
	uart_config();
	
	while(1)
	{
//		uart1_send_char('h');
		uart1_send_string ("Hello World!\r\n");
		delay_ms(1000);
//		uint8_t data = uart_get_char();
//		uart1_send_char (data);
	}
}