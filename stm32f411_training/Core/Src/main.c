/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

/* USER CODE BEGIN PV */
uint8_t a, b, sym_count = 16;
uint16_t digit_seg[8], segment[14];;
char text[] = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG        ";

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM3_Init(void);
/* USER CODE BEGIN PFP */
void write_data (uint8_t id_data, uint8_t cmd_data, uint16_t data_data);
void ht_1632c_init (void);
void set_symbol (uint8_t number);
void print_segment(uint16_t header, uint32_t number);
uint16_t convert_char (char word);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM4_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start_IT(&htim3);
	HAL_TIM_Base_Start_IT(&htim4);	
	ht_1632c_init();
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		HAL_GPIO_TogglePin(LED_IN_GPIO_Port, LED_IN_Pin);
		HAL_Delay(250);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 12;
  RCC_OscInitStruct.PLL.PLLN = 96;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 100-1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 0xffff-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 10000-1;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 1500-1;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_IN_GPIO_Port, LED_IN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, CS_Pin|WR_Pin|DATA_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LED_IN_Pin */
  GPIO_InitStruct.Pin = LED_IN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_IN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : CS_Pin WR_Pin DATA_Pin */
  GPIO_InitStruct.Pin = CS_Pin|WR_Pin|DATA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : LED_RED_Pin */
  GPIO_InitStruct.Pin = LED_RED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_RED_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim == &htim3)
	{
		print_segment(0,0);
	}
	if(htim == &htim4)
	{
		a = sizeof(text);
		int temp = text[0];
		for(int i=0; i<a-2; i++)
		{
			text[i] = text[i+1];
		}
		text[a-2] = temp;			
		
		//HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
		b = text[0];
		
		sym_count--;
		if(sym_count > 16) sym_count = 16;
	}
}

void write_data (uint8_t id_data, uint8_t cmd_data, uint16_t data_data)
{
	uint8_t temp;
	cs_low;	
	for(int i = 1 << 2; i > 0; i >>= 1)
	{
		temp = (id_data & i) ? 1 : 0;
		wr_low;
		HAL_GPIO_WritePin(DATA_GPIO_Port, DATA_Pin, temp);
		wr_high;
	}	
	switch(id_data)
	{
		case id_cmd:
			for(int i = 1 << 7; i > 0; i >>=1)
			{
				temp = (cmd_data & i) ? 1 : 0;
				wr_low;
				HAL_GPIO_WritePin(DATA_GPIO_Port, DATA_Pin, temp);
				wr_high;
			}
			wr_low;
			HAL_GPIO_WritePin(DATA_GPIO_Port, DATA_Pin, temp);
			wr_high;
			break;
		case id_write:
			for(int i = 1 << 6; i > 0; i >>=1)
			{
				temp = (cmd_data & i) ? 1 : 0;
				wr_low;
				HAL_GPIO_WritePin(DATA_GPIO_Port, DATA_Pin, temp);
				wr_high;
			}
			for(int i = 1 << 15; i > 0; i >>=1)
			{
				temp = (data_data & i) ? 1 : 0;
				wr_low;
				HAL_GPIO_WritePin(DATA_GPIO_Port, DATA_Pin, temp);
				wr_high;
			}
			break;
	}
	cs_high;
}

void ht_1632c_init (void)
{
	write_data(id_cmd, code_sys_dis, 0);
	write_data(id_cmd, code_com_nmos_16, 0);
	write_data(id_cmd, code_master_mode, 0);
	write_data(id_cmd, code_sys_en, 0);
	write_data(id_cmd, code_led_on, 0);
	for(int i = 0; i <= 0x5C; i+=4)
	{
		write_data(id_write, i, 0x0000);
	}
}

uint16_t convert_char (char word)
{
	switch(word)
	{
		case (0x30):
			return n_0;
			break;
		case (0x31):
			return n_1;
			break;
		case (0x32):
			return n_2;
			break;
		case (0x33):
			return n_3;
			break;
		case (0x34):
			return n_4;
			break;
		case (0x35):
			return n_5;
			break;
		case (0x36):
			return n_6;
			break;
		case (0x37):
			return n_7;
			break;
		case (0x38):
			return n_8;
			break;
		case (0x39):
			return n_9;
			break;
		case (0x41):
			return n_A;
			break;
		case (0x42):
			return n_B;
			break;
		case (0x43):
			return n_C;
			break;
		case (0x44):
			return n_D;
			break;
		case (0x45):
			return n_E;
			break;
		case (0x46):
			return n_F;
			break;
		case (0x47):
			return n_G;
			break;
		case (0x48):
			return n_H;
			break;
		case (0x49):
			return n_I;
			break;
		case (0x4A):
			return n_J;
			break;
		case (0x4B):
			return n_K;
			break;
		case (0x4C):
			return n_L;
			break;
		case (0x4D):
			return n_M;
			break;
		case (0x4E):
			return n_N;
			break;
		case (0x4F):
			return n_O;
			break;
		case (0x50):
			return n_P;
			break;
		case (0x51):
			return n_Q;
			break;
		case (0x52):
			return n_R;
			break;
		case (0x53):
			return n_S;
			break;
		case (0x54):
			return n_T;
			break;
		case (0x55):
			return n_U;
			break;
		case (0x56):
			return n_V;
			break;
		case (0x57):
			return n_W;
			break;
		case (0x58):
			return n_X;
			break;
		case (0x59):
			return n_Y;
			break;
		case (0x5A):
			return n_Z;
			break;
		default:
			return 0x0000;
			break;
	}
}

void set_symbol (uint8_t number)
{
	switch(number)
	{
		case(0):
			set_play;
			break;
		case(1):
			set_repeat;
			break;
		case(2):
			set_mic;
			break;
		case(3):
			set_bt;
			break;
		case(4):
			set_fm;
			break;
		case(5):
			set_card;
			break;
		case(6):
			set_usb;
			break;
		case(7):
			set_wifi;
			break;
		case(8):
			set_music;
			break;
		case(9):
			set_video;
			break;
		case(10):
			set_picture;
			break;
		case(11):
			set_pause;
			break;
		case(12):
			set_rand;
			break;
		case(13):
			set_L;
			break;
		case(14):
			set_R;
			break;
		case(15):
			set_bazz;
			break;
		case(16):
			set_mhz;
			break;		
		default:
			break;
	}
}

void print_segment(uint16_t header, uint32_t number)
{	
	digit_seg[0] = convert_char(text[0]);
	digit_seg[1] = convert_char(text[1]);
	digit_seg[2] = convert_char(text[2]);
	digit_seg[3] = convert_char(text[3]);
	digit_seg[4] = convert_char(text[4]);
	digit_seg[5] = convert_char(text[5]);
	digit_seg[6] = convert_char(text[6]);
	digit_seg[7] = convert_char(text[7]);	
	for(int i = 0; i < 14; i++)
		segment[i] = 0;	
	int p = 1;
	for(int i = 0; i < 14; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			segment[i] <<= 1;
			segment[i] = (digit_seg[j] & p) ? segment[i] | 1 : segment[i] | 0;
		}
		p = p << 1;
		segment[i] <<= 8;
	}		
	set_symbol(sym_count);	
	p = 0;
	for(int i = 0x00; i <= 0x34; i+=4)
	{
		write_data(id_write, i, segment[p]);
		p++;
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
